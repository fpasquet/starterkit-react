import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import configureStore from './shared/store';
import Root from './views/root';
import './assets/scss/main.scss';

const store = configureStore();
const syncedHistory = syncHistoryWithStore(browserHistory, store);
const rootElement = document.getElementById('root');

ReactDOM.render(
    <Root history={syncedHistory} store={store} />,
    rootElement
);