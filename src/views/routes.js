import App from './app';
import Home from './pages/home';

export const paths = {
  ROOT: '/',
  HOME: '/'
};

export const getRoutes = getState => {
  return {
    path: paths.ROOT,
    component: App,
    childRoutes: [
      {
        indexRoute: {
          component: Home
        }
      }
    ]
  };
};
